﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClooitAPI.Factory;
using ClooitAPI.Models;
using ClooitAPI.Plugins;
using ClooitAPI.Repositories;

namespace ClooitAPI.Controllers
{
    [RoutePrefix("Api/Stations")]
    public class StationsController : ApiController
    {
        private IStationRepository stationRepository;

        public StationsController()
        {
            stationRepository = new StationRepository(); // Strict initialization
        }

        /// <summary>
        /// Constructor to inject the station repository 
        /// </summary>
        /// <param name="repository"></param>
        public StationsController(IStationRepository repository)
        {
            stationRepository = repository;
        }

        /// <summary>
        /// GET api/stations
        /// Returns information about all the stations, registered in system
        /// </summary>
        /// <returns>List of station informations</returns>
        [HttpGet]
        [Route("", Order = 1, Name = "Top100")]
        [Route("{page:min(1)=1}-{count:range(1,100)=100}", Order = 2, Name = "WithLimit")]
        public List<Station> Get(int page = 1, int Count = 20)
        {
            //todo Optimize get with limit
            using (var context = new clooitEntities())
            {
                var stations = context.sp_GetStations();
                if (stations != null)
                {
                    return stations.Select(a => new Station()
                    {
                        ID = a.ID,
                        Description = a.Description,
                        Name = a.Name,
                        StationTypeName = a.StationTypeName,
                        Status = a.Status
                    }).ToList();
                }
            }
            return null;
        }

        [Route("{id:int}")]
        public Station GetAt(int id)
        {
            if (id == 0)
                return null;
            using (var context = new clooitEntities())
            {
                var station = context.sp_GetStationByID(id);
                if (station != null)
                {
                    return station.Select(a => new Station()
                    {
                        ID = a.ID,
                        Description = a.Description,
                        Name = a.Name,
                        StationTypeName = a.StationTypeName,
                        Status = a.Status
                    }).FirstOrDefault();
                }
            }
            return null;
        }


        [HttpPost]
        [AuthCheck]
        [Route("add")]
        public int? Add(TemperatureHumidityData temperature)
        {
            stationRepository.InitializeStation(temperature.StationID);
            // For now save only if station is "web" type. In other case for now station saves it data itself
            // So get the station type
            var stationInfo = this.GetAt(temperature.StationID ?? 0);
            if (stationInfo == null)
                return null; // todo throw some error
            if (stationInfo.StationTypeID == 1) // todo refactor this
            {
                var res = stationRepository.StoreStationMeasurements(temperature);
                if (res.HasValue)
                    return res;
            }
            return Request.GetStationInfo().ID;
        }

        [HttpPost]
        [AuthCheck]
        [Route("addRange")]
        public int? AddRange(TemperatureHumidityData[] temperatures)
        {
            stationRepository.InitializeStation(Request.GetStationInfo().ID);
            var res = temperatures.All(p => stationRepository.StoreStationMeasurements(p) > 0);
            if (res)
                return res ? 1 : 0;
            return Request.GetStationInfo().ID;
        }



        /// <summary>
        /// Test get temperature method
        /// </summary>
        /// <param name="stationID">The ID of the station to get the temerature data for</param>
        /// <param name="param">IParameter in JSON  format. todo maybe find a better way to do that</param>
        /// <returns></returns>
        [HttpGet]
        [Route("gettempbystation")]
        public TemperatureHumidityData GetTemperatureDataByStation(int stationID, string param)
        {
            // Get station properties by station id
            // todo this must be from properties!
            stationRepository.InitializeStation(stationID);
            stationRepository.GetStationAdapterType();
            try
            {
                ITemperatureEntity te = stationRepository.LoadStationAdapterType();
                if (te == null)
                    Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Station Type not defined, cant get the temperature entity");
                List<IParameter> parameters = stationRepository.DeserializeStationParameters(param);

                TemperatureHumidityData result = te.GetTemeperatureDataNow(parameters);
                if (result != null)
                {
                    result.StationID = stationID;
                }
                // We've got the result, lets save it. 
                // For web stations this part will save the temperature info to the store. 
                // For other types of stations - StoreStationMeasurements implementation can be different
                stationRepository.StoreStationMeasurements(result);

                return result;
            }
            catch (ArgumentNullException ane)
            {
                // todo handle the exception; log?
                Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Station Type not defined");
            }

            return null;
        }

    }
}
