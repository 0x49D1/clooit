﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ClooitAPI.Models;

namespace ClooitAPI.Controllers
{
    public class GroupStationsController : ApiController
    {
        /// <summary>
        /// Get stations, assigned to group
        /// </summary>
        /// <param name="groupid">Group to search stations for</param>
        /// <returns>List of assigned stations </returns>
        public List<GroupStation> Get(int groupid)
        {
            using (var context = new clooitEntities())
            {
                var stations = context.sp_GetGroupStations(groupid);
                if (stations != null)
                {
                    var lst = stations.ToList();
                    if (lst.Count > 0)
                        return lst.Select(a => new GroupStation()
                        {
                            ID = (int)a.ID,
                            Description = a.Description,
                            Name = a.Name,
                            StationTypeName = a.StationTypeName,
                            Status = (bool)a.Status,
                            GroupID = groupid
                        }).ToList();
                }
            }
            return null;
        }
    }
}
