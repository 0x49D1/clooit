﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net;
using System.Net.Http;
namespace ClooitAPI.Factory
{
    public static class ContexParameters
    {
        internal  const string StationInfo = "Save-Station-Info";
        public static sp_GetStationByName_Result GetStationInfo(this HttpRequestMessage param)
        {
            object p = null;
            param.Properties.TryGetValue(StationInfo, out p);
            return p as sp_GetStationByName_Result;
        }
    }


    public class AuthCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext context)
        {
            try
            {
                var secret = context.Request.Headers.GetValues("secret").Single();
                var user = context.Request.Headers.GetValues("client-name").Single();
                using (var db = new clooitEntities())
                {
                    var client = db.sp_GetStationByName(user).Single(p => p.Status);
                    db.sp_GetStationParameters(user).Single(p => "secret" == secret);
                    context.Request.Properties.Add(ContexParameters.StationInfo,client);
                }
            }
            catch (Exception ex)
            {
                context.Response = context.Request
               .CreateErrorResponse(HttpStatusCode.NotAcceptable,ex);
            }
        }
    }
}