﻿namespace ClooitAPI.Plugins.OpenWeatherMap.Models
{
    public class Wind
    {
        public float speed { get; set; }
        public float deg { get; set; }
    }
}