﻿using System;
using System.Collections.Generic;
using System.Linq;
using ClooitAPI.Common;
using ClooitAPI.Models;
using ClooitAPI.Plugins.OpenWeatherMap.Models;

namespace ClooitAPI.Plugins.OpenWeatherMap
{
    /// <summary>
    /// To show the conditions open weather map needs City and 2 letter country code. 
    /// </summary>
    public class OpenWeatherMapAdapter : ITemperatureEntity
    {
        // todo for now this works only for tbilisi
        public TemperatureHumidityData GetTemeperatureDataNow(List<IParameter> inputParameters)
        {
            var idParameter = inputParameters.FirstOrDefault(a => string.Compare(a.Key, "ID", StringComparison.InvariantCultureIgnoreCase) == 0);
            IParameter cityParameter = null;
            IParameter countryCodeParameter = null;
            IParameter latParameter = null;
            IParameter lonParameter = null;

            if (idParameter == null)
            {
                cityParameter = inputParameters.FirstOrDefault(a => string.Compare(a.Key, "City", StringComparison.InvariantCultureIgnoreCase) == 0);
                if (cityParameter == null)
                {
                    //throw new ArgumentException("City was not provided");
                }
                countryCodeParameter = inputParameters.FirstOrDefault(a => string.Compare(a.Key, "Code", StringComparison.InvariantCultureIgnoreCase) == 0);
                if (countryCodeParameter == null)
                {
                    //throw new ArgumentException("Country was not provided");
                }
                latParameter =
                    inputParameters.FirstOrDefault(
                        a => string.Compare(a.Key, "lat", StringComparison.InvariantCultureIgnoreCase) == 0);
                lonParameter =
                    inputParameters.FirstOrDefault(
                        a => string.Compare(a.Key, "lon", StringComparison.InvariantCultureIgnoreCase) == 0);
            }
            Rootobject res = null; // todo refactor 
            if (idParameter != null)
                res = HttpHelper.GETJson<Rootobject>("http://api.openweathermap.org/data/2.5/weather?id=" + idParameter.Value);
            else if (cityParameter != null)
                res = HttpHelper.GETJson<Rootobject>("http://api.openweathermap.org/data/2.5/weather?q=" + cityParameter.Value + "," + countryCodeParameter.Value);
            else if (latParameter != null && lonParameter != null)
                res = HttpHelper.GETJson<Rootobject>("http://api.openweathermap.org/data/2.5/weather?lat=" + latParameter.Value + "&lon=" + lonParameter.Value);

            if (res != null)
            {
                return new TemperatureHumidityData()
                {
                    TemperatureValue = TemperatureConverter.KelvinToCelsius(res.main.temp),
                    HumidityValue = res.main.humidity,
                    Latitude = res.coord.lat,
                    Longtitude = res.coord.lon,
                    Description = res.name
                };
            }
            return null;
        }

        public List<IParameter> GetStationParameters()
        {
            var l = new List<IParameter>();
            l.Add(new CommonParameter() { Key = "City", Type = ParameterType.String, Value = "Tbilisi", Mask = @"^\w+$" });
            l.Add(new CommonParameter() { Key = "Code", Type = ParameterType.String, Value = "GE", Mask = @"^\w{2}$" });
            l.Add(new CommonParameter() { Key = "ID", Type = ParameterType.String, Value = "611717", Mask = @"^\d+$" });

            return l;
        }
    }

    public class Parameter : IParameter
    {
        public string Key
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public ParameterType Type
        {
            get;
            set;
        }

        public string Mask
        {
            get;
            set;
        }

        public bool IsRequired
        {
            get;
            set;
        }

        public List<Dictionary<string, string>> ReferenceList
        {
            get;
            set;
        }
    }
}