﻿using System.Collections.Generic;
using ClooitAPI.Models;

namespace ClooitAPI.Plugins
{
    public interface ITemperatureEntity : IStationBase
    {
        TemperatureHumidityData GetTemeperatureDataNow(List<IParameter> inputParameters);
    }
}
