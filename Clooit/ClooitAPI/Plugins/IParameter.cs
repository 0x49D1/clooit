using System.Collections.Generic;

namespace ClooitAPI.Plugins
{
    public interface IParameter
    {
        /// <summary>
        /// Parameter name
        /// </summary>
        string Key { get; set; }
        /// <summary>
        /// Value/default value/example value
        /// </summary>
        string Value { get; set; }
        /// <summary>
        /// Type to guess what type of control can represent the parameter
        /// </summary>
        ParameterType Type { get; set; }
        /// <summary>
        /// Regular expression mask for the parameter
        /// </summary>
        string Mask { get; set; }
        /// <summary>
        /// Is this a required parameter?
        /// </summary>
        bool IsRequired { get; set; }
        /// <summary>
        /// If the parameter is list - here are possible list items
        /// </summary>
        List<Dictionary<string, string>> ReferenceList { get; set; }
    }
}