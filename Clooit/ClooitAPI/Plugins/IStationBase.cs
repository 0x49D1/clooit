﻿using System.Collections.Generic;

namespace ClooitAPI.Plugins
{
    public interface IStationBase
    {
        /// <summary>
        /// Returns the parameters, required to get weather information from the station
        /// </summary>
        /// <returns>The list of parameters with "descriptions"</returns>
        List<IParameter> GetStationParameters();

    }
}