using System.Collections.Generic;

namespace ClooitAPI.Plugins
{
    public class CommonParameter : IParameter
    {
        /// <summary>
        /// By default the parameter is required
        /// </summary>
        private bool _isRequired = true;

        public string Key
        {
            get;
            set;
        }

        public string Value
        {
            get;
            set;
        }

        public ParameterType Type
        {
            get;
            set;
        }

        public string Mask { get; set; }

        /// <summary>
        /// All the parameters are required by default
        /// </summary>
        public bool IsRequired
        {
            get { return _isRequired; }
            set { _isRequired = value; }
        }

        public List<Dictionary<string,string>> ReferenceList { get; set; }

    }
}