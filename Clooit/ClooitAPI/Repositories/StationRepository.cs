﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using ClooitAPI.Models;
using ClooitAPI.Plugins;
using Newtonsoft.Json;

namespace ClooitAPI.Repositories
{
    public class StationRepository : IStationRepository
    {
        public int StationID { get; set; }

        public string AdapterType
        {
            get;
            set;
        }

        public void InitializeStation(int? stationID)
        {
            // Possibly change to lazy load instead of this
            if (!stationID.HasValue)
            {
                return;// Throw some exception
            }
            StationID = stationID.Value;
        }

        public string GetStationAdapterType()
        {
            string type = string.Empty;
            using (var context = new clooitEntities())
            {
                var t = context.sp_GetStationParametersByID(StationID).ToList();
                if (t.Count > 0)
                {
                    var tmp = t.FirstOrDefault(a => a.ParameterType == 2); // Getting the adapter
                    if (tmp != null)
                        type = tmp.Value;
                }
            }
            if (string.IsNullOrEmpty(type))
            {
                return null; // Thow some exception, becase of not implemented adapter for the station(?)
            }

            AdapterType = type;// Set the adapter type property

            return type;
        }

        public ITemperatureEntity LoadStationAdapterType()
        {
            Type elementType = Type.GetType(AdapterType);
            if (elementType == null)
                throw new ArgumentNullException("Cant get type for the adapter " + AdapterType);
                // Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Station Type not defined");
           

            ITemperatureEntity te = (ITemperatureEntity)Activator.CreateInstance(elementType); // Get this parameter using reflection?
            return te;
        }

        public List<IParameter> DeserializeStationParameters(string param)
        {
            // Lets introduce the parameter naming convention: ClooitAPI.Plugins.OpenWeatherMap.Parameter = adapter type with latest section changed to Parameter fot the type so:
            //string parametersType = AdapterType.Remove(AdapterType.LastIndexOf('.')) + ".Parameter";
            //Type elementType = Type.GetType(parametersType);
            //var a = (IParameter)elementType; :(
            List<IParameter> parameters = new List<IParameter>(JsonConvert.DeserializeObject<List<ClooitAPI.Plugins.CommonParameter>>(param)); // Fix typing here
            return parameters;
        }

        public int? StoreStationMeasurements(TemperatureHumidityData td)
        {
            // Save the measurement into the database
            using (var context = new clooitEntities())
            {
                return context.sp_AddTemperatureData(StationID, td.TemperatureValue, "C", td.HumidityValue, td.Latitude??0, td.Longtitude??0, td.Altitude??0);
            }
        }
    }

    public interface IStationRepository
    {
        int StationID { get; set; }
        string AdapterType { get; set; }
        //List<IParameter> StationParameters { get; set; }
        void InitializeStation(int? stationID); // DP Think that int? is better, because then we can use various exceptions for not implemented 
        /// <summary>
        /// Gets the adapter tyepe for current station
        /// </summary>
        /// <returns>Adapter type as a string for future adapter load</returns>
        string GetStationAdapterType();
        /// <summary>
        /// Loads the type for the current station and returns it back
        /// </summary>
        /// <returns>The loaded adapter type for current plugin</returns>
        ITemperatureEntity LoadStationAdapterType();
        /// <summary>
        /// Choose proper serialization method for the station
        /// </summary>
        /// <param name="param">Parameters as string</param>
        /// <returns>List of properly serialized parameters</returns>
        List<IParameter> DeserializeStationParameters(string param);

        int? StoreStationMeasurements(TemperatureHumidityData td);

    }
}