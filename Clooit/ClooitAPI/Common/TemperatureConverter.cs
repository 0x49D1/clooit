﻿namespace ClooitAPI.Common
{
    public static class TemperatureConverter
    {
        private const double cAbsTempC = 273.15;//absolute temperature in Celcius
        private const double cAbsTempF = 459.67;//absolute temperature in Fahrenheit

        public static double KelvinToCelsius(double kelvin)
        {
            return kelvin - cAbsTempC;
        }

        public static double CelsiusToKelvin(double celsius)
        {
            return celsius + cAbsTempC;
        }

        public static double FahrenheitToCelsius(double fahrenheit)
        {
            return FahrenheitToKelvin(fahrenheit) - cAbsTempC;
        }

        public static double FahrenheitToKelvin(double fahrenheit)
        {
            return (fahrenheit + cAbsTempF) * 5 / 9;
        }
    }
}