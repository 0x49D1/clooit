﻿using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ClooitAPI.Common
{
    public class HttpHelper
    {
        public enum SerializationType
        {
            JSON,
            XML,
            RAW
        }

        public static TResponse POST<TRequest, TResponse>(SerializationType serializationType, string url, TRequest obj, string contentType = "", X509Certificate certificate = null)
            where TRequest : class
            where TResponse : class
        {
            if (serializationType == SerializationType.XML)
            {
                if (string.IsNullOrEmpty(contentType))
                    contentType = "application/x-www-form-urlencoded";

                return POSTXmlSerialization<TRequest, TResponse>(url, obj, contentType, certificate);
            }
            else
            {
                if (string.IsNullOrEmpty(contentType))
                    contentType = "application/json";

                return POSTJsonSerialization<TRequest, TResponse>(url, obj, contentType, certificate);
            }
        }

        #region XML Serialization block

        private static TResponse POSTXmlSerialization<TRequest, TResponse>(string url, TRequest obj, string contentType,
            X509Certificate certificate)
            where TRequest : class
            where TResponse : class
        {
            // 1. Serialize
            var serializer = new XmlSerializer(typeof(TRequest));
            var sww = new StringWriter();
            var writer = XmlWriter.Create(sww);
            serializer.Serialize(writer, obj);
            var post_data = sww.ToString(); // Your xml

            //if (post_data.Contains(XML_HEADER))
            //    post_data = post_data.Replace(XML_HEADER, XML_HEADER2);
            //    post_data = post_data.Remove(0, XML_HEADER.Length);

            // 2. Send Request
            var response = POSTXml(url, post_data, contentType, certificate);


            // 3. Deserialize
            /*  var responseXML = new StreamReader(response).ReadToEnd();
              var byteArray = Encoding.ASCII.GetBytes(responseXML);
              var stream = new MemoryStream(byteArray); */
            var deserializer = new XmlSerializer(typeof(TResponse));

            return (deserializer.Deserialize(response) as TResponse);
        }

        public static Stream POSTXml(string url, string post_data,
                    string contentType = "application/x-www-form-urlencoded", X509Certificate certificate = null)
        {

            // create a request
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";

            if (certificate != null)
                request.ClientCertificates.Add(certificate);


            // turn our request string into a byte stream
            var postBytes = Encoding.ASCII.GetBytes(post_data);

            // this is important - make sure you specify type this way
            request.ContentType = contentType; //text/xml
            request.ContentLength = postBytes.Length;
            var requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code

            string bufStr = (new StreamReader(((HttpWebResponse)request.GetResponse()).GetResponseStream())).ReadToEnd();
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(bufStr));
            return stream;
        }

        /// <summary>
        /// Makes HTTP GET request and expects XML to deserialize it into requested object type 
        /// </summary>
        /// <typeparam name="TResponse">Object type for XML deserialization</typeparam>
        /// <param name="url">URL to send GET request to</param>
        /// <returns>Deserialized object, returned from request</returns>
        public static TResponse GETXml<TResponse>(string url)
            where TResponse : class
        {
            // Send request and download response as string
            var byteArray = new WebClient().DownloadData(url);

            var stream = new MemoryStream(byteArray);
            var deserializer = new XmlSerializer(typeof(TResponse));

       
            return (deserializer.Deserialize(stream) as TResponse);
        }

        #endregion

        #region JSON Serialization block

        /// <summary>
        /// POST request with json serialized data
        /// </summary>
        /// <param name="url">Where to send POST</param>
        /// <param name="post_data">Data to send(JSON serialized string)</param>
        /// <param name="contentType">Content type(JSON by default)</param>
        /// <param name="certificate">Certificate if needed</param>
        /// <returns>Response stream</returns>
        public static Stream POSTJson(string url, string post_data, string contentType = "application/json", X509Certificate certificate = null)
        {
            // create a request
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";

            if (certificate != null)
                request.ClientCertificates.Add(certificate);

            // turn our request string into a byte stream
            var postBytes = Encoding.UTF8.GetBytes(post_data);

            // this is important - make sure you specify type this way
            request.ContentType = contentType; //application/json
            request.ContentLength = postBytes.Length;
            var requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code
            var response = (HttpWebResponse)request.GetResponse();

            return response.GetResponseStream();
        }

        /// <summary>
        /// Send POST request and get response, then deserealize it into TResponse type
        /// </summary>
        /// <typeparam name="TRequest">Request object type</typeparam>
        /// <typeparam name="TResponse">Response object type</typeparam>
        /// <param name="url">URL to send POST request</param>
        /// <param name="obj">Object that will be JSON serialized and send with POST</param>
        /// <param name="contentType">Content type, JSON by default</param>
        /// <param name="certificate">Certificate if needed</param>
        /// <returns>Deserialized response stream into object of type TResponse</returns>
        public static TResponse POSTJsonSerialization<TRequest, TResponse>(string url, TRequest obj, string contentType = "application/json", X509Certificate certificate = null)
            where TRequest : class
            where TResponse : class
        {
            // 1. Serialize
            var serializer = JsonConvert.SerializeObject(obj);

            var post_data = serializer;

            // 2. Send Request
            var response = POSTJson(url, post_data, contentType, certificate);

            // 3. Deserialize
            var responseString = new StreamReader(response).ReadToEnd();
            return (JsonConvert.DeserializeObject<TResponse>(responseString));
        }


        /// <summary>
        /// Makes HTTP GET request and expects JSON to deserialize it into requested object type 
        /// </summary>
        /// <typeparam name="TResponse">Object type for JSON deserialization</typeparam>
        /// <param name="url">URL to send GET request to</param>
        /// <returns>Deserialized object, returned from request</returns>
        public static TResponse GETJson<TResponse>(string url)
            where TResponse : class
        {
            // Send request and download response as string
            var byteArray = new WebClient().DownloadData(url);
            string result = System.Text.Encoding.UTF8.GetString(byteArray);
            return JsonConvert.DeserializeObject<TResponse>(result);
        }

        /// <summary>
        /// Makes HTTP POST request and expects JSON to deserialize it into requested object type 
        /// </summary>
        /// <typeparam name="TResponse">Object type for JSON deserialization</typeparam>
        /// <param name="url">URL to send POST request to</param>
        /// <param name="parameters">POST content to be sent</param>
        /// <returns>Deserialized object, returned from request</returns>
        public static TResponse POSTJson<TResponse>(string url, NameValueCollection parameters)
    where TResponse : class
        {
            byte[] byteArray = new WebClient().UploadValues(url, parameters);
            var result = System.Text.Encoding.UTF8.GetString(byteArray);
            return JsonConvert.DeserializeObject<TResponse>(result);
        }

        #endregion


        #region RAW Serialization block

        private static string POSTRawSerialization(string url, string post_data, string contentType,
            X509Certificate certificate)
        {
            var response = POSTRaw(url, post_data, contentType, certificate);


            return new StreamReader(response).ReadToEnd();
        }

        public static Stream POSTRaw(string url, string post_data,
                    string contentType = "application/x-www-form-urlencoded", X509Certificate certificate = null)
        {

            // create a request
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version11;
            request.Method = "POST";


            if (certificate != null)
                request.ClientCertificates.Add(certificate);


            // turn our request string into a byte stream
            var postBytes = Encoding.ASCII.GetBytes(post_data);

            // this is important - make sure you specify type this way
            request.ContentType = contentType; //text/xml
            request.ContentLength = postBytes.Length;
            var requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code

            string bufStr = (new StreamReader(((HttpWebResponse)request.GetResponse()).GetResponseStream())).ReadToEnd();
            MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(bufStr));
            return stream;
        }

        /// <summary>
        /// Makes HTTP GET request and expects XML to deserialize it into requested object type 
        /// </summary>
        /// <typeparam name="TResponse">Object type for XML deserialization</typeparam>
        /// <param name="url">URL to send GET request to</param>
        /// <returns>Deserialized object, returned from request</returns>
        public static string GETRaw(string url)
        {
            // Send request and download response as string
            var byteArray = new WebClient().DownloadData(url);
            var result = Encoding.UTF8.GetString(byteArray);
    
            return result;
        }

        #endregion

    }
}