﻿using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClooitAPI.Models
{
    public class FullStationResponse
    {
        /// <summary>
        /// ID of the station we search information for
        /// </summary>
        public int StationID { get; set; }
        /// <summary>
        /// User friendly name
        /// </summary>
        public string StationName { get; set; }
        /// <summary>
        /// Type of the station 
        /// </summary>
        public int StationTypeID { get; set; }
        /// <summary>
        /// Data list for the station. It can be LIST, because it can be taken for date range
        /// </summary>
        public List<TemperatureHumidityData> Data { get; set; }
    }
}