﻿namespace ClooitAPI.Models
{
    public class GroupStation : Station
    {
        /// <summary>
        /// ID of the group we take the station information for
        /// </summary>
        public int GroupID { get; set; }
    }
}