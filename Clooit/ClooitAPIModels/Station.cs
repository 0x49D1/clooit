﻿namespace ClooitAPI.Models
{
    public class Station
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public string StationTypeName { get; set; }
        public int StationTypeID { get; set; }
    }
}