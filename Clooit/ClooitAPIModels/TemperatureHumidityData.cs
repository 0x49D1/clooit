﻿using System;

namespace ClooitAPI.Models
{
    /// <summary>
    /// Temperature and humidity values scheme
    /// </summary>
    public class TemperatureHumidityData
    {
        /// <summary>
        /// Temperature measured in Celsius!
        /// </summary>
        public double? TemperatureValue { get; set; }
        /// <summary>
        /// Humidity measured in %!
        /// </summary>
        public int? HumidityValue { get; set; }
        /// <summary>
        /// Latitude value if it was measured
        /// </summary>
        public double? Latitude { get; set; }
        /// <summary>
        /// Longtitude value if it was measured
        /// </summary>
        public double? Longtitude { get; set; }
        /// <summary>
        /// Altitude value if it was measured
        /// </summary>
        public double? Altitude { get; set; }
        /// <summary>
        /// Station where the data was measured
        /// </summary>
        public int? StationID { get; set; }
        /// <summary>
        /// Measurement datetime in GMT+0!
        /// </summary>
        public DateTime MeasurementUTCDate { get; set; }

        public string Description { get; set; }
    }
}