 interface Led{
	  toggle():void;
 }
 interface Output{
	  output(Number):Led
 }
 interface IUse{
	 on(command:string,fn:(err?:Error)=>void);
	 readTemperature(command:string,fn:(err:Error,temp:number)=>void);
	 readHumidity(fn:(err:Error,humid:number)=>void);
 }
 
 interface WifiConfig{
	ssid:string, password:string,  security:string, timeout :string;
 }
 
 //declare function setImmediate(fn?:()=>void);
 
 declare module "tessel"{
	export var led:Array<Output>; 
	export var port:Array<any>;
}

 declare module "climate-si7020"{
	export function use(port:any):IUse;
}

 declare module "wifi-cc3000"{
	 export function connect(config:WifiConfig,fn:(err:Error,result:any)=>void);
	 export function on(command:string,fn:(err:Error,data:any)=>void);
	 export function isBusy():boolean;
	 export function isConnected():boolean;
 }