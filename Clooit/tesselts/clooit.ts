/// <reference path='typings/node/node.d.ts' />
/// <reference path='typings/tessel/tessel.d.ts' />
/// <reference path='typings/needle/needle.d.ts'/>
import tessel = require("tessel");
import config = require("./config");
import wifi = require('wifi-cc3000');
import needle = require("needle");
import climateLib = require('climate-si7020');


var led1 = tessel.led[0].output(1);
var led=tessel.led[1].output(1);



var WIFI_CONNECT_STARTED: number = 0;
var WiFI_CONNECT_CHECK_TIMEOUT: number = 40000;
var elipsTime: number = 0;
var climateIsReady: boolean = false;
var httpOption={
	 headers:{
		"secret":config.options.secret,
	 	"client-name":config.options.station_name},
		 json:true};
		 
var temperatures: Array<TemperaturePush> = new Array<TemperaturePush>();
var climate = climateLib.use(tessel.port[config.moduleID]); 


class TemperaturePush{
	 constructor(public TemperatureValue:string,public HumidityValue:number,public MeasurementUTCDate:string){		
	}
};

var getDateTimeSync = () => {
    return new Date((new Date().getTime() - elipsTime)).toISOString();
};

var fixWifiConnection = () => {
    if (wifi.isConnected())
        return;
    if (!wifi.isBusy() && config.wifi) {
        if (WIFI_CONNECT_STARTED > 0) {
            console.log("Another connection is started wait...")
            setTimeout(fixWifiConnection, WiFI_CONNECT_CHECK_TIMEOUT);
            return;
        }
        WIFI_CONNECT_STARTED++;
        console.log("Connect to wifi");
        wifi.connect(config.wifi,(err, res) => {
            WIFI_CONNECT_STARTED--;
            console.log("(C_C)");
            if (err) {
                console.error(err.message);
            } else {
                console.log("Wifi connected");
            }
        });
        console.log("Wifi is busy");
    } else {
        setTimeout(fixWifiConnection, WiFI_CONNECT_CHECK_TIMEOUT);
    }
};

var updateClimate = () => {
    if (climateIsReady)
        try {
            console.log("update climate");
            climate.readTemperature('c',(err, temp) => {
                if (!err)
                    try {
                        console.log(temp.toFixed(4) + 'C')
                        climate.readHumidity((err, humid) => {
                            if (!err) {
                                try {
                                    console.log(humid.toFixed(4) + "h%");
                                    temperatures.push(new TemperaturePush(temp.toFixed(4),
                                        humid|0,
                                        getDateTimeSync()
                                        ));
                                }
                                catch (ex) {
                                    console.log(ex);
                                }
                            } else
                                console.log(err);

                        });
                    } catch (ex) {
                        console.error(ex)
                    }
                else
                    console.error(err);

            });
        }
        catch (ex) {
            console.error(ex);
        }
    setTimeout(updateClimate, config.sendPeriod);
};
	
var sendDataTo = () => {
    try {
        console.log("start Send data :)");
        led.toggle();
        if (temperatures && temperatures.length > 0 && wifi.isConnected()) {
            var req = temperatures;
            temperatures = new Array<TemperaturePush>();
            console.log("start Request:", config.options.URL + "/Stations/addRange");
            console.log(JSON.stringify(req));
            needle.post(config.options.URL + "/Stations/addRange", req,httpOption,(err, data) => {
                console.log("stations/addRange");
                if (err) {
                    console.error(err);
                }
                else {
                    console.log(data);
                }
            });
        }
    }
    catch (ex) {
        console.error(ex);
    }
    console.log("send data egain " + config.sendPeriod);
    setTimeout(sendDataTo, config.sendPeriod);
};

//wifi disconnect fix
wifi.on("disconnect",(err, data) => {
    console.log("disconected :(");
    if (err) {
        console.error(err);
    }
    if (data)
        console.log(data);
    console.log("Disconnect fix");
    fixWifiConnection();
});

climate.on("error",(err) => {
    console.log("climate :(");
    if (err)
        console.error(err);
});

climate.on("ready",() => {
    console.log("climate is ready");
    climateIsReady = true;
    setTimeout(updateClimate, config.sendPeriod);
    setTimeout(sendDataTo, config.sendPeriod);
}); 
console.log("init...");
if (config.sendPeriod < 2000)
    config.sendPeriod = 2000;

fixWifiConnection();

setTimeout(sendDataTo, config.sendPeriod);