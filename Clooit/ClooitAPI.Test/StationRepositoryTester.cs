﻿using System;
using ClooitAPI.Repositories;
using Newtonsoft.Json;
using NUnit.Framework;

namespace ClooitAPI.Test
{
    [TestFixture]
    public class StationRepositoryTester
    {
        // todo Initializations needed for moq repository. Concrete tests without initialization are below

        [Test]
        public void CheckJsonDeserializationForConcreteStationRepositoryShouldReturnTrue()
        {
            // This check is for StationRepository concrete repository only. Check can be different for other implementations. 
            // So we test the concrete JSON deserialization here
            StationRepository concreteStationRepository = new StationRepository();
            var param = concreteStationRepository.DeserializeStationParameters("[{'key':'city','value':'Tbilisi'},{'key':'code','value':'GE'}]"); // Must check that json can be properly deserialized to IParameter
            // Must be IParameters list of objects with Tbilisi/GE
            Assert.IsTrue(param != null, "Cant deserialize thestring");
            Assert.IsTrue(param.Count == 2, "Not properly deserialized the parameters");
            Assert.IsTrue(string.Compare(param[0].Value, "tbilisi", StringComparison.InvariantCultureIgnoreCase) == 0, "At least one parameter was not properly deserialized");
        }

        [Test(Description = "Fault for incorrect exception")]
        [ExpectedException(typeof(JsonReaderException))] // OK in case of exception
        public void CheckIncorrectJsonDeserializationForConcreteStationRepositoryShouldReturnFalse()
        {
            StationRepository concreteStationRepository = new StationRepository(); 
            concreteStationRepository.DeserializeStationParameters("[{'ky':'city','value':'Tbilisi'},{'key':'code','value':'GE}]");

            Assert.IsFalse(true, "FAIL, serialization was successfull for incorrect json string"); // Fail in case execution is here
        }
    }
}
