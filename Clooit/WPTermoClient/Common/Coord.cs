﻿namespace ClooitSampleWPClient.Common
{
    public class Coord
    {
        public float lon { get; set; }
        public float lat { get; set; }
    }
}