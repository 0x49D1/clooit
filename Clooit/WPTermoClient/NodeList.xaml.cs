﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using ClooitAPI.Models;
using ClooitSampleWPClient.Common;
using Newtonsoft.Json;
using RestSharp.Portable;

namespace ClooitSampleWPClient
{
    public sealed partial class NodeList : Page
    {
        private NavigationHelper navigationHelper;
        StatusBar statusBar = StatusBar.GetForCurrentView();
        private string APILOCATION = "apilocation";
        private string SELECTEDSTATION = "selectedstation";

        public NodeList()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.Loaded += (sender, args) =>
            {
                cmbStation.Visibility = Visibility.Collapsed;
                tbStationHeader.Visibility = Visibility.Collapsed;
                //txtWeatherStationName.Text = "http://api.openweathermap.org/data/2.5/weather?id=611717"; // "http://nodepingx.azurewebsites.net/?get=ping"; 
                txtApiLocation.Text = "http://clooittestapi.azurewebsites.net/api/";

                var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
                settings.Values.Clear();
                settings.Values.Add(APILOCATION, txtApiLocation.Text);
            };

        }

        /// <summary>
        /// Adds an item to the list when the app bar button is clicked.
        /// </summary>
        private void AddAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            if (cmbStation.SelectedItem != null)
            {
                Windows.Storage.ApplicationData.Current.LocalSettings.Values.Add(SELECTEDSTATION,
                    JsonConvert.SerializeObject(cmbStation.SelectedItem));
                Frame.Navigate(typeof (Dashboard));
            }
        }

        private void BtnGetStations_OnClick(object sender, RoutedEventArgs e)
        {
            var res = GetStations();
            //cmbStation.ItemsSource = res;

            //tbInfo.Text = result.ToString();
        }

        private async Task<List<Station>> GetStations()
        {
            using (RestClient client = new RestClient())
            {
                client.BaseUrl = new Uri(Windows.Storage.ApplicationData.Current.LocalSettings.Values[APILOCATION].ToString());

                RestRequest request = new RestRequest("stations", HttpMethod.Get);
                statusBar.ProgressIndicator.ShowAsync();
                var res = await client.Execute<List<Station>>(request);
                cmbStation.ItemsSource = res.Data;
                statusBar.ProgressIndicator.HideAsync();
                cmbStation.Visibility = tbStationHeader.Visibility = Visibility.Visible;
                return res.Data as List<Station>;
            }
            return null;
        }
    }
}
