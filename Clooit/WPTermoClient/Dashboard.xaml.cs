﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using ClooitAPI.Models;
using ClooitSampleWPClient.Common;
using Newtonsoft.Json;
using RestSharp.Portable;

// The Pivot Application template is documented at http://go.microsoft.com/fwlink/?LinkID=391641

namespace ClooitSampleWPClient
{
    public sealed partial class Dashboard : Page
    {
        StatusBar statusBar = StatusBar.GetForCurrentView();

        private string APILOCATION = "apilocation";// todo refactor
        private string SELECTEDSTATION = "selectedstation";
        public Station SelectedStation { get; set; }

        public Dashboard()
        {
            this.InitializeComponent();

            this.Loaded += (sender, args) =>
            {
                SelectedStation =
                    JsonConvert.DeserializeObject<Station>(
                        ApplicationData.Current.LocalSettings.Values[SELECTEDSTATION].ToString());
                GetCurrentMeasurement();
            };
        }

        private async void GetCurrentMeasurement()
        {

            statusBar.ProgressIndicator.ShowAsync();

            RestClient client = new RestClient(new Uri(ApplicationData.Current.LocalSettings.Values[APILOCATION].ToString()));
            RestRequest request = new RestRequest("stations/gettempbystation", HttpMethod.Get);
            request.AddQueryParameter("stationid", SelectedStation.ID);
            request.AddQueryParameter("param", "[{'Key':'City','Value':'Tbilisi'},{'Key':'Code','Value':'GE'}]");

            var res = await client.Execute<TemperatureHumidityData>(request);
            ShowReponse(res.Data);
        }

        private void ShowReponse(TemperatureHumidityData res)
        {
            try
            {
                double tem = res.TemperatureValue.Value; // To celsium tempData.c;
                double h = res.HumidityValue.Value;

                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => txtTemp.Text = (tem).ToString("F2") + "C");
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => txtHum.Text = (h).ToString("F1") + "%");

                Color c = Colors.White;
                // Change the color based on "comfort"
                List<string> messagesTemperature = new List<string>();
                List<string> messagesHumidity = new List<string>();

                if (tem < 5)
                {
                    c = Colors.DodgerBlue;
                    messagesTemperature.Add("Brrr... Its cold!");
                    messagesTemperature.Add("Ice queen is ready waiting!");
                    messagesTemperature.Add("Brrr...!");
                    messagesTemperature.Add("Yaaay, snowflakes!");
                }
                else if (tem < 20)
                {
                    c = Colors.Orange;
                    messagesTemperature.Add("You can wear the spring things!");
                    messagesTemperature.Add("Its just warm!");
                }
                else if (tem < 35)
                {
                    c = Colors.OrangeRed;
                    messagesTemperature.Add("Weather is comfy!");
                    messagesTemperature.Add("Its defenetely spring or summer!");
                    messagesTemperature.Add("Listen.. Children are happily playing football outside!");
                    messagesTemperature.Add("Suitup, its warm!");
                    messagesTemperature.Add("You can even wear shorts!");
                    messagesTemperature.Add("Summer has always been my favorite season. I feel happier.");
                }
                else
                {
                    c = Colors.Red;
                    messagesTemperature.Add("You will DIE. The sun is angry!");
                }
                if (h > 95)
                    messagesHumidity.Add(" Hope you know how to swim!");
                if (h > 70)
                    messagesHumidity.Add(" You probably need an umbrella!");
                else
                    messagesHumidity.Add("");

                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => txtMessage.Text = messagesTemperature[new Random().Next(messagesTemperature.Count)] + messagesHumidity[new Random().Next(messagesHumidity.Count)]);
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => txtDescripion.Text = res.Description);

                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => txtTemp.Foreground = new SolidColorBrush(c));
                Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => statusBar.HideAsync());
            }
            catch (WebException e)
            {
                return;
            }

        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            GetCurrentMeasurement();
        }
    }

    public class TempData
    {
        public double Temperature { get; set; }
        public double Humidity { get; set; }

        public void TempDataAdapter(string content)
        {
            // Deserialize, based on input URL
            string url = ApplicationData.Current.LocalSettings.Values["weatherstation"].ToString();


            if (url.Contains("api.openweathermap.org"))
            {
                var a = JsonConvert.DeserializeObject<Rootobject>(content);
                Temperature = a.main.temp - 273.15;
                Humidity = a.main.humidity;
            }
            else if (url.Contains("nodepingx.azurewebsites.net"))
            {
                var a = JsonConvert.DeserializeObject<RootobjectLocal>(content);
                Temperature = a.c;
                Humidity = a.h;
            }
        }
    }
}

